package main

import (
	"bytes"
	"fmt"
	"time"

	"github.com/umahmood/haversine"
)

var TimeToWalk = map[string]time.Duration{}

type AnswerStep struct {
	Tot    TrickOrTreater
	Arrive time.Time
}

type CacheAnswer struct {
	Path []AnswerStep
	Time time.Time
}

// algorithm constants

var algocache = map[string]CacheAnswer{}

var TimeSpentAtTOT time.Duration

var MinutesPerMileWalk time.Duration

func AlgorithmStep(tot []TrickOrTreater, coordinate haversine.Coord, ignoreDistance bool, arrive time.Time) ([]AnswerStep, time.Time) {
	cachestrb := new(bytes.Buffer)
	for _, t := range tot {
		fmt.Fprintf(cachestrb, "%s.", t.Department)
	}
	fmt.Fprintf(cachestrb, "%f.%f.%s", coordinate.Lat, coordinate.Lon, arrive)

	cachestr := cachestrb.String()
	answer, ok := algocache[cachestr]
	if ok {
		return answer.Path, answer.Time
	}

	if len(tot) == 0 {
		// base case
		end := arrive
		return []AnswerStep{}, end
	}

	/*
		1: path of trick or treaters
		2: time at which the path ends (tiebreaker for same max score)
	*/
	var maxPath []AnswerStep
	var fastestEnd time.Time = time.Date(2500, 1, 1, 0, 0, 0, 0, time.Local)

	for i, v := range tot {
		/*
			We have to copy the slice because when we pop it will break
		*/
		tot2 := make([]TrickOrTreater, len(tot))
		copy(tot2, tot)
		/*
			totExclude := tot2[:i]
			totExclude = append(totExclude, tot2[i+1:]...)
		*/
		totExclude := tot2[i+1:]
		arriveTime := arrive
		/*
			If not ignore distance, account for how much time
			it takes to walk to that destination
		*/
		if !ignoreDistance {
			distanceMiles, _ := haversine.Distance(coordinate, v.Coord)
			timeToWalk := float64(MinutesPerMileWalk) * distanceMiles
			arriveTime = arriveTime.Add(time.Duration(timeToWalk))
		}
		if v.Start.After(arriveTime) {
			arriveTime = v.Start
		}
		if arriveTime.After(v.End) {
			continue // this station has closed by the time we got there.
		}

		// get the time after spending time at this station
		timeAfter := arriveTime.Add(TimeSpentAtTOT)

		// if totExclude is length 0, we are done.
		// have to set a final time to the value of timeAfter

		path := []AnswerStep{AnswerStep{
			Tot:    v,
			Arrive: arriveTime,
		}}
		pathAfter, endTime := AlgorithmStep(totExclude, v.Coord, false, timeAfter)
		path = append(path, pathAfter...)
		if len(path) > len(maxPath) || (len(path) == len(maxPath) && fastestEnd.After(endTime)) {
			// new path is larger (higher score) OR same size path AND ends faster
			// found a better path
			maxPath = path
			fastestEnd = endTime
		}

	}
	algocache[cachestr] = CacheAnswer{maxPath, fastestEnd}
	return maxPath, fastestEnd
}

module codeberg.org/FiskFan1999/oberlinTrickOrTreat2022

go 1.19

require (
	github.com/armon/go-metrics v0.0.0-20180917152333-f0300d1749da // indirect
	github.com/hashicorp/go-immutable-radix v1.0.0 // indirect
	github.com/hashicorp/golang-lru v0.5.0 // indirect
	github.com/hashicorp/serf v0.10.1 // indirect
	github.com/umahmood/haversine v0.0.0-20151105152445-808ab04add26 // indirect
)

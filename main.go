package main

import (
	"encoding/csv"
	"encoding/xml"
	"flag"
	"fmt"
	"os"
	"sort"
	"strconv"
	"strings"
	"time"

	"github.com/umahmood/haversine"
)

const totFile = "gall.csv"

/*
Reading locations.kml
*/
const LocationsInfo = "locations.kml"

type LocationsTop struct {
	Document struct {
		Placemarks []struct {
			Name  string `xml:"name"`
			Point struct {
				Coordinates string `xml:"coordinates"`
			} `xml:"Point"`
		} `xml:"Placemark"`
	} `xml:"Document"`
}

var POI = map[string]haversine.Coord{
	"38 E College St": {41.29195381944652, -82.21615921813147},
}

type Coordinates string

type TrickOrTreater struct {
	Department string
	CoordOrig  string
	Coord      haversine.Coord
	Start      time.Time
	End        time.Time
}

/*
Graph is an undirected weighted graph
in which each node has a start
and end time
*/
type TOTstr []TrickOrTreater

func (t TOTstr) Len() int {
	return len(t)
}

func (t TOTstr) Less(i, j int) bool {
	if t[j].End.After(t[i].End) {
		return true
	}
	if t[j].End == t[i].End {
		return t[j].Start.After(t[i].Start)
	}
	return false
}

func (t TOTstr) Swap(i, j int) {
	a := (t)[j]
	(t)[j] = t[i]
	(t)[i] = a
}

var TrickOrTreaters = TOTstr{}

func main() {
	/*
		Read flags
	*/
	flag.DurationVar(&MinutesPerMileWalk, "m", time.Minute*19, "Walking speed: minutes per mile")
	flag.DurationVar(&TimeSpentAtTOT, "t", time.Minute*10, "How much time is spent at each station")
	flag.Parse()
	/*
		Read locations
	*/
	locationBytes, err := os.ReadFile(LocationsInfo)
	if err != nil {
		panic(err)
	}
	var locationsTop LocationsTop
	if err := xml.Unmarshal(locationBytes, &locationsTop); err != nil {
		panic(err)
	}
	for _, place := range locationsTop.Document.Placemarks {
		coordsStrip := strings.TrimSpace(place.Point.Coordinates)
		coordsSpl := strings.Split(coordsStrip, ",")
		ystr := coordsSpl[0]
		xstr := coordsSpl[1]

		x, err := strconv.ParseFloat(xstr, 64)
		if err != nil {
			panic(err)
		}
		y, err := strconv.ParseFloat(ystr, 64)
		if err != nil {
			panic(err)
		}

		POI[place.Name] = haversine.Coord{x, y}
	}
	/*
		Read trick-or-treating locations
	*/

	totFile, err := os.Open(totFile)
	if err != nil {
		panic(err)
	}
	defer totFile.Close()

	csvr := csv.NewReader(totFile)
	csvr.Read()
	toters, err := csvr.ReadAll()
	if err != nil {
		panic(err)
	}

	var totline []string
	for _, totline = range toters {
		name := totline[0]
		place := totline[1]
		timestr := totline[2]

		pplace, ok := placeToCoords[place]
		if !ok {
			fmt.Println("--- not found ---")
			fmt.Printf("%q\n", place)
			os.Exit(0)
		}
		coords, ok := POI[pplace]
		if !ok {
			fmt.Println("Error: pplase not found")
			fmt.Printf("%q -> %q\n", place, pplace)
			os.Exit(0)
		}

		start, end := GetTOTStartEnd(timestr)

		TrickOrTreaters = append(TrickOrTreaters, TrickOrTreater{
			Department: name,
			CoordOrig:  place,
			Coord:      coords,
			Start:      start,
			End:        end,
		})
	}

	/*
		RUN ALGORITHM
	*/

	sort.Stable(TrickOrTreaters)

	if false {
		TrickOrTreaters = TrickOrTreaters[:20] // smaller size for testing
	}

	path, endTime := AlgorithmStep(TrickOrTreaters, haversine.Coord{}, true, time.Date(1, 1, 1, 0, 0, 0, 0, time.Local))
	fmt.Println("endTime=", endTime)
	for i, p := range path {
		fmt.Printf("%d: (%s) %s, %s (%s-%s) \n", i+1, p.Arrive.Format(time.Kitchen), p.Tot.Department, p.Tot.CoordOrig, p.Tot.Start.Format(time.Kitchen), p.Tot.End.Format(time.Kitchen))
	}

}

func GetTOTStartEnd(timestr string) (start, end time.Time) {
	spl := strings.Split(timestr, "-")
	startstr := strings.TrimSpace(spl[0])
	endstr := strings.TrimSpace(spl[1])

	var ok bool
	start, ok = timeMap[startstr]
	if !ok {
		fmt.Printf("Not found: %q\n", startstr)
		os.Exit(0)
	}
	end, ok = timeMap[endstr]
	if !ok {
		fmt.Printf("Not found: %q\n", endstr)
		os.Exit(0)
	}
	return start, end
}

var timeMap = map[string]time.Time{
	"1":         time.Date(2022, 11, 2, 13, 0, 0, 0, time.Local),
	"1:30":      time.Date(2022, 11, 2, 13, 30, 0, 0, time.Local),
	"2":         time.Date(2022, 11, 2, 14, 0, 0, 0, time.Local),
	"3":         time.Date(2022, 11, 2, 15, 0, 0, 0, time.Local),
	"4":         time.Date(2022, 11, 2, 16, 0, 0, 0, time.Local),
	"5":         time.Date(2022, 11, 2, 17, 0, 0, 0, time.Local),
	"1 P.M.":    time.Date(2022, 11, 2, 13, 0, 0, 0, time.Local),
	"2 P.M.":    time.Date(2022, 11, 2, 14, 0, 0, 0, time.Local),
	"3 P.M.":    time.Date(2022, 11, 2, 15, 0, 0, 0, time.Local),
	"4 P.M.":    time.Date(2022, 11, 2, 16, 0, 0, 0, time.Local),
	"5 P.M.":    time.Date(2022, 11, 2, 17, 0, 0, 0, time.Local),
	"1 p.m.":    time.Date(2022, 11, 2, 13, 0, 0, 0, time.Local),
	"2 p.m.":    time.Date(2022, 11, 2, 14, 0, 0, 0, time.Local),
	"2:30 p.m.": time.Date(2022, 11, 2, 14, 0, 0, 0, time.Local),
	"3 p.m.":    time.Date(2022, 11, 2, 15, 0, 0, 0, time.Local),
	"3:00pm":    time.Date(2022, 11, 2, 15, 0, 0, 0, time.Local),
	"4 p.m.":    time.Date(2022, 11, 2, 16, 0, 0, 0, time.Local),
	"4:00 p.m.": time.Date(2022, 11, 2, 16, 0, 0, 0, time.Local),
	"4:30 p.m.": time.Date(2022, 11, 2, 16, 30, 0, 0, time.Local),
	"5 p.m.":    time.Date(2022, 11, 2, 17, 0, 0, 0, time.Local),
	"5:00 p.m.": time.Date(2022, 11, 2, 17, 0, 0, 0, time.Local),
	"10 a.m.":   time.Date(2022, 11, 2, 10, 0, 0, 0, time.Local),
}

var placeToCoords = map[string]string{
	"Mudd A-Level":             "Mudd Center",
	"Stevenson Hall, Room 100": "Stevenson Hall",
	"Rice, Room 130":           "Rice Hall",
	"Peters, Room 103":         "Peters Hall",
	"Wilder, Room 111":         "Wilder Hall",
	"38 E College St":          "38 E College St",
	"Service Buildling (173 W Lorain Street), Room 208 ": "Service Building",
	"Carnegie, Room 204":                             "Carnegie Building",
	"Carnegie, 4th floor landing":                    "Carnegie Building",
	"Carnegie, Room 122":                             "Carnegie Building",
	"Carnegie, Room 113":                             "Carnegie Building",
	"Carnegie, Room 123":                             "Carnegie Building",
	"Carnegie, Room 124":                             "Carnegie Building",
	"Science Center K123":                            "Science Center",
	"Science Center A263":                            "Science Center",
	"Science Center K125 ":                           "Science Center",
	"Science Center N174":                            "Science Center",
	"Wilder Hall, Room 105":                          "Wilder Hall",
	"Wilder, Room 208":                               "Wilder Hall",
	"Wilder, Room 212":                               "Wilder Hall",
	"Service Buildling (173 W. Lorain St., Ste. 205": "Service Building",
	"Stevenson Hall, Griswold Commons, 155 N. Professor St.": "Stevenson Hall",
	"Stevenson Dining Hall in the Griswold entrance":         "Stevenson Hall",
	"Conservatory Central Unit 33":                           "Bibbins Hall",
	"Mudd Center 205":                                        "Mudd Center",
	"Bibbins 113":                                            "Bibbins Hall",
	"Rice basement (Rice 29)":                                "Rice Hall",
	"Conservatory Annex 206":                                 "Bibbins Hall",
}
